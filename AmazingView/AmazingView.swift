//
//  AmazingView.swift
//  AmazingView
//
//  Created by deepak on 05/12/22.
//

import UIKit

public class AmazingView: UIView {
    
    //MARK: - PROPERTIES AND INITIALIZERS
    let colors : [UIColor] = [.red, .orange, .yellow, .green, .blue, .purple]
    var colorCounter = 0

    //MARK: - UIVIEW INITIALIZERS
    override init(frame: CGRect) {
        super.init(frame: frame)
            
        let scheduledColorChanged = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { timer in
            UIView.animate(withDuration: 2.0) {
                self.layer.backgroundColor = self.colors[self.colorCounter % 6].cgColor
                self.colorCounter += 1
            }
        }
        scheduledColorChanged.fire()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
     
        // You don't need to implement this
    }
    
    public func scheduleColorChange() {
        let scheduledColorChanged = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { timer in
            UIView.animate(withDuration: 2.0) {
                self.layer.backgroundColor = self.colors[self.colorCounter % 6].cgColor
                self.colorCounter += 1
            }
        }
        scheduledColorChanged.fire()
    }

}
