//
//  ViewController.swift
//  AmazingView
//
//  Created by deepak on 05/12/22.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - UIVIEW LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let amazingView = AmazingView(frame: self.view.bounds)
        self.view.addSubview(amazingView)
    }


}

